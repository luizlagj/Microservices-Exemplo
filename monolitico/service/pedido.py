# ! /usr/bin/python
from flask import request
from flask_restful import Resource
from flask import Flask
from flask_restful import Api
from pymongo import MongoClient


app = Flask(__name__)

api = Api(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JSON_AS_ASCII'] = False


class Pedido(Resource):

    def post(self):
        json_data = request.get_json()
        print(json_data)
        cliente = busca_cliente(json_data['cliente']['cpf'])
        if cliente is not None:
            pedido = json_data['pedido'].copy()
            pedido['cpf'] = json_data['cliente']['cpf']
            cria_pedido(pedido)
            return None, 204
        else:
            return None, 500


def busca_cliente(cpf):
    client = MongoClient('localhost', 27017)
    database = client.local
    cliente_db = database.cliente
    cliente = cliente_db.find_one({"cpf": cpf})
    return cliente


def cria_pedido(pedido):
    client = MongoClient('localhost', 27017)
    database = client.local
    pedido_db = database.pedido
    pedido_id = pedido_db.insert_one(pedido).inserted_id
    print(pedido_id)


api.add_resource(Pedido, '/pedido')


if __name__ == '__main__':
    app.run('0.0.0.0', 8083)
