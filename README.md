# Dois exemplos que ilustram um pouco o benefício de uma arquitetura voltada a microserviços

1. Monolítico utilizando um orquestrador(ESB) [aqui](https://bitbucket.org/luijunior/microservices-exemplo/src/master/monolitico/)
2. Microserviços utilizando coreografia e eventos [aqui](https://bitbucket.org/luijunior/microservices-exemplo/src/master/microservico/)


### Teste wrk

#### Monolitico

Criar o arquivo com os dados do Post

```bash
printf "wrk.method = 'POST' \n \
wrk.body = '{\"cliente\":{\"cpf\":\"12312312312\",\"nome\":\"Teste\"},\"pedido\":{\"valor\":150,\"produto\":\"AMPM\"}}' \n \
wrk.headers[\"Content-Type\"] = 'application/json' \n" > post.lua
```

Efetuar request

```bash
wrk -t3 -c10 -d10s -s ./post.lua http://localhost:8082
```

#### Microservicos

Criar o arquivo com os dados do Post

```bash
printf "wrk.method = 'POST' \n \
wrk.body = '{\"cliente\":{\"cpf\":\"12312312312\",\"nome\":\"Teste\"},\"pedido\":{\"valor\":150,\"produto\":\"AMPM\"}}' \n \
wrk.headers[\"Content-Type\"] = 'application/json' \n \
wrk.headers[\"evento\"] = 'criarPedido' " > post.lua
```

Efetuar request

```bash
wrk -t3 -c10 -d10s -s ./post.lua http://localhost:8091
```