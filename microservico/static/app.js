var app = new Vue({
    el: '#app',
    data: {
        cpf: '',
        nome: '',
        produto: '',
        valor: 0,
        mensagem: '',
        sucesso: false,
        criar: '',
        mostrarLoad: false,
        mostrarBotao: true,
        mostrarForm: true
    },
    methods: {
        logSucesso: function () {
            console.log(this.sucesso);
        },
        criarPedido() {
            this.mostrarBotao = false;
            this.mostrarLoad = true;
            this.mostrarForm = false;

            var data = {
                "cliente": {
                    "cpf": this.cpf,
                    "nome": this.nome
                },
                "pedido": {
                    "valor": parseFloat(this.valor),
                    "produto": this.produto
                }
            }
            $.ajax({
                type: "POST",
                url: "http://localhost:8091",
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: "json",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "evento": "criarPedido"
                },
                error: function (jqXHR, status) {
                    this.sucesso = false;
                    this.mostrarLoad = false;
                    this.mensagem = "Erro ao criar pedido";
                    this.mostrarForm = true;
                }
            }).done(data => {
                this.mostrarLoad = false;
                this.sucesso = true;
                this.mensagem = "Pedido criado com sucesso";
                this.mostrarForm = false;
            });
        }
    }
})