# ! /usr/bin/python
from flask import Flask
import pika
import json
import redis
import time


app = Flask(__name__)


def credita_pontos(body):
    # time.sleep(10)
    if float(body['pedido']['valor']) > 100.0:
        r = redis.Redis(host='localhost', port=6379, db=0)
        r.incr('cpf:%s' % body['cliente']['cpf'], amount=50)


def consome_fila(ch, method, properties, body):
    if 'criarPedido' in method.routing_key:
        credita_pontos(json.loads(body.decode('utf8')))
        ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == '__main__':
    credentials = pika.credentials.PlainCredentials('luiz', 'luiz')
    connection = pika.BlockingConnection(
        pika.ConnectionParameters('localhost',
                                  credentials=credentials))
    channel = connection.channel()

    channel.basic_consume(on_message_callback=consome_fila,
                          queue='pontos',
                          auto_ack=False)
    channel.start_consuming()
