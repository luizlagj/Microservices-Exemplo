# ! /usr/bin/python
from flask import Flask
import pika
import json
from elasticsearch import Elasticsearch


app = Flask(__name__)


def criar_pedido(body):
    els = Elasticsearch()
    els.index(index='transacao', body=body, doc_type='pedido')


def consome_fila(ch, method, properties, body):
    if 'criarPedido' in method.routing_key:
        criar_pedido(json.loads(body.decode('utf8')))
        ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == '__main__':
    credentials = pika.credentials.PlainCredentials('luiz', 'luiz')
    connection = pika.BlockingConnection(
        pika.ConnectionParameters('localhost',
                                  credentials=credentials))
    channel = connection.channel()

    channel.basic_consume(on_message_callback=consome_fila,
                          queue='pedido',
                          auto_ack=False)
    channel.start_consuming()
