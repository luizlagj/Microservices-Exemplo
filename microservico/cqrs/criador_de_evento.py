# ! /usr/bin/python
import json
from flask import request
from flask_restful import Resource, reqparse
from flask import Flask
from flask_restful import Api
from flask_cors import CORS
import pika


app = Flask(__name__)

api = Api(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JSON_AS_ASCII'] = False
CORS(app)


class CriadorDeEvento(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('evento', required=True, type=str,
                            help='Evento a ser criado', location='headers')
        args = parser.parse_args()
        json_data = request.get_json()
        credentials = pika.credentials.PlainCredentials('luiz', 'luiz')
        connection = pika.BlockingConnection(
            pika.ConnectionParameters('localhost',
                                      credentials=credentials))
        channel = connection.channel()
        channel.basic_publish(exchange='amq.direct',
                              routing_key=args['evento'],
                              body=json.dumps(json_data))
        return None, 200


api.add_resource(CriadorDeEvento, '/')


if __name__ == '__main__':
    app.run('0.0.0.0', 8091)
