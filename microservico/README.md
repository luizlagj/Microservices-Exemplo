# Dependencias

1. Docker
2. Python3
3. Anaconda

## Startar Backend

### Startar Rabbitmq

```bash
docker run -d -p 15672:15672 -p 5672:5672 --hostname rabbit-microservicos --name rabbit-microservicos -e RABBITMQ_DEFAULT_USER=luiz -e RABBITMQ_DEFAULT_PASS=luiz rabbitmq:3-management
```

#### Criar as filas e efetuar bind

Acessar

http://localhost:15672/#/

Criar queues

* cliente
* pedido
* pontos

Efetuar o binding do exchange **amq.direct** utilizando o rounting key **criarPedido** para as filas criadas acima

### Startar MongoDB

#### Banco que sera utilizado pelo microservico de cliente

```bash
docker run --name mongo-microservicos -p 27017:27017 -d mongo:3.2.21-jessie
```

### Startar o Elasticsearch

#### Banco que sera utilizado pelo microservico de pedido

```bash
docker run -d -p 9200:9200 --name elasticsearch-microservicos docker.elastic.co/elasticsearch/elasticsearch:6.5.0
```

Caso der erro executar o comando abaixo e depois tentar novamente

```bash
sudo -S sysctl -w vm.max_map_count=262144
```

### Startar o Redis

#### Banco que sera utilizado pelo microservico de pontos

```bash
docker run -d -p 6379:6379 --name redis-microservicos -d redis
```

## Startar aplicações

```bash
# Carregar o ambiente
conda env create -f environment.yml
source activate microservicos
```

```bash
cd <HOME_APP_DIR>
python app.py
python ./cqrs/criador_de_evento.py
python ./consumidor/consumidor_cliente.py
python ./consumidor/consumidor_pedido.py
python ./consumidor/consumidor_pontos.py
```

### Abrir no browser home de criacao de pedido

http://localhost:8090



### Para verificar a criacao de clientes

```bash
docker exec -it mongo-microservicos /bin/bash
mongo
use local
# Consultar todos os clientes
db.cliente.find()
# Remover todos os clientes
db.cliente.deleteMany({})
```

### Para verificar a criacao de pedidos

```
wget http://localhost:9200/transacao/pedido/_search?q=cliente.cpf=<CPF DO CLIENTE>
```

### Para verificar o credito de pontos

```bash
docker exec -it redis-microservicos /bin/bash
redis-cli
keys cpf*
get cpf:<cpf do cliente>
```